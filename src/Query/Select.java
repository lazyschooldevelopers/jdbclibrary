/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Query;

import java.sql.*;
import java.util.*;
import java.util.logging.LogManager;

import com.google.common.base.*;
import util.*;
import Connection.*;
/**
 * TO USE THIS CLASS YOU NEED TO DOWNLOAD AND IMPORT THE "guava-18.0" CLASS
 * @author Yannick
 */
public class Select extends QueryBase {
    
    public Select(String query, JavaConnection connection){
        _query = query;
        super.connection = connection;
        super._parameters = new LinkedList();
    }




    protected ResultSet executeQuery() throws SQLException{
        PreparedStatement pstm = CreatePreparedStatement.create(_query, _parameters, connection.getConnection());
        try{
            return pstm.executeQuery();
        }catch(SQLException ex){
//            LogManager.log(ERROR, ex.getMessage())
            throw ex;
        }

    }
}
