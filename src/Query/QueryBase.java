package Query;

import Connection.JavaConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Created by yanni on 1/13/2017.
 */
abstract public class QueryBase<T> {
    protected String _query;
    protected LinkedList _parameters;
    protected JavaConnection connection;

    public void addRecords(String[] recordNames){
        for(int i = 0; i < recordNames.length; i++)
            addRecord(recordNames[i] + (i == (recordNames.length-1) ? "" : ", "));
    }

    public void addRecord(String recordName){
        _query += recordName;
    }

    public void addTables(String[] tableNames){
        _query += " FROM ";
        for(int i = 0; i < tableNames.length; i++)
            addTable(tableNames[i] + (i == tableNames.length-1 ? "" : ", "));
    }

    public void addTable(String tableName){
        if(!_query.contains("FROM"))
            _query += " FROM ";
        _query += tableName;
    }

    public void addParameters(String[] parameterNames, LinkedList parameters) throws Exception{
        _query += " WHERE ";
        if(parameterNames.length != parameters.size())
            throw new Exception("The parameters has not the same length.");
        for(int i = 0; i < parameterNames.length; i++){
            addParameter(parameterNames[i], parameters.get(i));
            _query += (i != parameterNames.length-1 ? ", " : "");
        }
    }

    public void addParameter(String parameterName, Object parameter){
        _parameters.add(parameter);
        _query +=parameterName +" = ?";
    }

    protected abstract T executeQuery() throws SQLException;
}
