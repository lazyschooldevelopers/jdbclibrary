/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Query;
import Connection.*;

/**
 * TO USE THIS CLASS YOU NEED TO DOWNLOAD AND IMPORT THE "guava-18.0" CLASS
 * @author Yannick
 */
import com.google.common.base.Joiner;
import java.util.*;
import java.sql.*;
import java.util.logging.LogManager;

import util.CreatePreparedStatement;

public class Update extends QueryBase {

    public Update(String query, JavaConnection connection){
        super._query = query;
        super.connection = connection;
        super._parameters = new LinkedList();
    }

    protected Integer executeQuery() throws SQLException{
        PreparedStatement pstmt = CreatePreparedStatement.create(_query, _parameters, connection.getConnection());
        try{
            return  pstmt.executeUpdate();
        } catch(SQLException ex){
//            LogManager.log(ERROR, ex.getMessage());
            throw ex;
        }
    }
}
