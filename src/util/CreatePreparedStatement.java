/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.*;

/**
 *
 * @author Yannick
 */
public class CreatePreparedStatement {

    /**
     * Create a new PreparedStatement with parameters
     * @param query
     * @param parameters
     * @param connection
     * @return
     * @throws SQLException
     */
    public static PreparedStatement create (String query, LinkedList parameters, Connection connection) throws SQLException{
        PreparedStatement pstmt = connection.prepareStatement(query);




        int i = 1;
        for(Object o : parameters){
            try{
                pstmt.getClass().getMethod("set" + o.getClass(), o.getClass()).invoke(pstmt, i, o.getClass().cast(o));
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            i++;
        }
        return pstmt;
    }
}
