/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Yannick
 */
public class TableJoinColumn {
    
    // <editor-fold desc="DataMembers">
          private String _tableColumnName;  
    // </editor-fold>

          
    // <editor-fold desc="Ctor">
          
          public TableJoinColumn(String columnName){
              _tableColumnName = columnName;
          }
          
    // </editor-fold>
          
    // <editor-fold desc="Public Methods">
          public String getTableColumnName(){
              return _tableColumnName;
          }
          
          public void setTableColumnName(String columnName){
              _tableColumnName = columnName;
          }
    // </editor-fold>
    
    
}
