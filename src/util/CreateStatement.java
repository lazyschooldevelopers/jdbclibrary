package util;

import java.sql.*;

/**
 *
 * @author Yannick
 */
public class CreateStatement {
    
    /**
     * Create a new Statement
     * @param connection
     * @return
     * @throws SQLException 
     */
    public static Statement create(Connection connection) throws SQLException{
        try{
            Statement stmt = connection.createStatement();
            return stmt;
        }catch(SQLException ex){
            //LogManager.log(ERROR, ex.getMessage());
            throw ex;
        }
    }
    
}
