package Connection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Yannick
 */

import java.sql.*;
import java.util.logging.LogManager;

public class JavaConnection {
    
    private static final String _driver = "com.mysql.jdbc.Driver";

    private Connection connection;
     
     /**
      * Create the connection
      * @param url like "domain/DatabaseName"
      * @return
      * @throws SQLException 
      */
     
     public void connect(String url) throws SQLException{
         
         try{
             Class.forName(_driver);
         }catch(ClassNotFoundException e){
             System.err.println(e.getCause());
             
         }
         
         connection = DriverManager.getConnection(url, "root", "");

     }

     public Connection getConnection(){
         return connection;
     }

     public void closeConnection(){
         try{
             connection.close();
         }catch(SQLException ex){
//             LogManager.log(ERROR, ex.getMessage());
         }
     }
    
}
