/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableAndDB;

/**
 *
 * @author Yannick
 */

import java.util.*;
import java.sql.*;
import java.util.logging.LogManager;

import util.CreateStatement;

public class TableCreation {
    private static final String _query = "CREATE TABLE ";
    
    /**
     * Create a new table with attributes
     * @param name
     * @param attributes
     * @param connection
     * @return
     * @throws SQLException 
     */
    public static boolean create(String name, ArrayList attributes, Connection connection) throws SQLException{
        String query = _query + name + "(";

        if(attributes.size() == 0)
            return create(name, connection);

        for(Object o : attributes){
            String str = (String)o;
            query = query + o;
        }
        query = query + ")";

        Statement stmt = CreateStatement.create(connection);

        try{
            return stmt.execute(query);
        }catch(SQLException ex){
            //LogManager.log(Error, ex.getMessage());
            throw ex;
        }
    }
    
    /**
     * Create a new void table
     * @param name
     * @param connection
     * @return
     * @throws SQLException 
     */
    private static boolean create (String name, Connection connection) throws SQLException{
        String query = _query + name;
        
        Statement stmt = CreateStatement.create(connection);

        try{
            return stmt.execute(query);
        }catch(SQLException ex){
            //LogManager.log(Error, ex.getMessage());
            throw ex;
        }
    }
}
