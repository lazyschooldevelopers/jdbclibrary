package TableAndDB;

/**
 *
 * @author Yannick
 */
import java.sql.*;
import util.CreateStatement;

public class DatabaseCreation {
    
    private static final String _query = "CREATE DATABASE ";
    
    /**
     * Create a new Database
     * @param name
     * @param connection
     * @return
     * @throws SQLException 
     */
    public static boolean create(String name, Connection connection) throws SQLException{
        String query = _query + name;
        Statement stmt = CreateStatement.create(connection);
        try{
            return stmt.execute(query);
        }
        catch(java.sql.SQLException ex){
            //LogManager.log(ERROR, ex.getMessage());
            throw ex;
        }
    }
}
